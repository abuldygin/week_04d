package ru.edu;

import ru.edu.screens.MainScreen;
import ru.edu.screens.Screen;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Stack;

public class Application {

    private final InputStream in;
    private final PrintStream out;
    private final Competition competition;
    private Screen mainScreen;
    private Stack<Screen> screens;

    public Application(InputStream in, PrintStream out, Competition competition, Screen mainScreen) {
        this.in = in;
        this.out = out;
        this.competition = competition;
        screens = new Stack<>();
        screens.push(mainScreen);
    }

    public void run() {
        while (screens.size() > 0){
            runScreen();
        }


    }

    void runScreen() {
        Screen screen = screens.peek();
        screen.promt();//выводим на экран что-то
        Screen newScreen = screen.readInput(competition);//считываем пользовательский ввод, производим действия
        if(newScreen != null){
            if(newScreen != screen){
                screens.push(screen);//добавим новый экран в стек
            }
        }else {
            screens.pop(); //удалим текущий экран из стека
        }
    }
}
