package ru.edu.screens;

import ru.edu.Competition;

public interface Screen {

    /**
     * Выводит текст на экран и подсказывает какой ввод ожидается.
     */
    void promt();

    /**
     * Считаем пользовательский ввод, если требуется.
     * Выполним действия.
     * Вернем this если остаемся на этом экране, null если возвращаемся назад.
     *
     * @return
     * @param competition
     */
    Screen readInput(Competition competition);

}
